package com.shopping.cart.shoppingcart.config;

import com.shopping.cart.shoppingcart.dao.CartDao;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CartConfig {

    @Bean
    CommandLineRunner commandLineRunner (CartDao cartDao){
        return args -> cartDao.initializeProducts();
    }
}
