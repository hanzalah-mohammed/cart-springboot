package com.shopping.cart.shoppingcart.dao;

import com.shopping.cart.shoppingcart.model.CartItem;
import com.shopping.cart.shoppingcart.model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CartDao {

    // create empty product list which act as a database
    Map<Long, Product> productDB = new HashMap<>();

    // initialize all the products in the productDB
    public void initializeProducts(){
        productDB.put(101L, new Product(101, "beef", 6.90));
        productDB.put(102L, new Product(102, "chicken", 7.20));
        productDB.put(103L, new Product(103, "fish", 8.60));
    }

    // return all products in the productDB
    public Map<Long, Product> getAllProducts() {
        return productDB;
    }

    // create empty cart list
    List<CartItem> cart = new ArrayList<>();

    // return all the items in cart
    public List<CartItem> getCartItems(){
        return cart;
    }

    // add item in cart with quantity or without quantity
    public void addItemInCart(Long productID, Integer quantityInput){

        CartItem existingItem = checkItemExistInCart(productID); // check item exist in cart or not.
        int quantity = quantityInput != null ? quantityInput : 1; // set the quantity as quantityInput. if null, default is 1.

        if(existingItem != null){ // if the item exists in cart
            existingItem.setQuantity(existingItem.getQuantity() + quantity); // change the quantity
            existingItem.setUnitPrice(existingItem.getProduct().getPrice() * existingItem.getQuantity()); // change the price
        } else { // if the item not exists in cart
            Product product = fetchProduct(productID); // call the fetchProduct() method
            double price = product.getPrice() * quantity;
            CartItem item = new CartItem(product, quantity, (double) (Math.round(price * 100) / 100)); // 3 variables -> Product, quantity & totalPrice
            cart.add(item); // put the cartItem inside cart
        }
    }

    // check whether the item exists in the cart or not. if yes, return the cartItem.
    public CartItem checkItemExistInCart(Long productID){
        for(CartItem cartItem : cart){ // iterate through all the products in the cart,
            if(cartItem.getProduct().getProductID() == productID){ // if it matches with productID from the user input,
                return cartItem; // then return the whole cartItem.
            }
        }
        return null; // if no product exists in productDB with the following productID, then return null
    }

    // fetch the product from productDB using productID
    public Product fetchProduct(Long productID){
        return productDB.get(productID);
    }

    public void updateDetails(Long productID, Integer quantity){
        CartItem existingItem = checkItemExistInCart(productID);

        if(existingItem != null){
            existingItem.setQuantity(quantity);
            existingItem.setUnitPrice(existingItem.getProduct().getPrice() * quantity);
        }
    }

    // remove 1 quantity for one product
    public void removeItem(Long productID) {
        CartItem existingItem = checkItemExistInCart(productID);

        if(existingItem != null){
            int quantity = existingItem.getQuantity();
            if (quantity > 1){
                existingItem.setQuantity(existingItem.getQuantity() - 1);
                existingItem.setUnitPrice(existingItem.getProduct().getPrice() * existingItem.getQuantity());
            } else {
                cart.remove(existingItem);
            }
        } else {
            throw new IllegalStateException("Item with product ID " + productID + " not available in the cart!!");
        }
    }

    // remove all quantity for one product
    public void removeItemAll(Long productID){
        cart.removeIf(e -> e.getProduct().getProductID() == productID);
    }

    // remove all products in cart
    public void clearCart() {
        cart.clear();
    }

    // getTotalPrice for all the items in cart
    public double getTotalPrice(){
        return cart.stream().mapToDouble(CartItem::getUnitPrice).sum();
    }
}
