package com.shopping.cart.shoppingcart.model;

import java.util.Objects;

public class CartItem {

    private Product product;
    private Integer quantity;
    private Double totalUnitPrice;

    public CartItem(Product product, Integer quantity, Double totalUnitPrice) {
        this.product = product;
        this.quantity = quantity;
        this.totalUnitPrice = totalUnitPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getUnitPrice() {
        return totalUnitPrice;
    }

    public void setUnitPrice(Double totalUnitPrice) {
        this.totalUnitPrice = totalUnitPrice;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "product=" + product +
                ", quantity=" + quantity +
                ", totalUnitPrice=" + totalUnitPrice +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartItem cartItem = (CartItem) o;
        return Objects.equals(product, cartItem.product) && Objects.equals(quantity, cartItem.quantity) && Objects.equals(totalUnitPrice, cartItem.totalUnitPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, quantity, totalUnitPrice);
    }
}
