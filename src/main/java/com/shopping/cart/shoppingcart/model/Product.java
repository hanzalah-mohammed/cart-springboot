package com.shopping.cart.shoppingcart.model;

public class Product {

    private Long productID;
    private String name;
    private Double unitPrice;

    public Product(long productID, String name, double unitPrice) {
        this.productID = productID;
        this.name = name;
        this.unitPrice = unitPrice;
    }

    public long getProductID() {
        return productID;
    }

    public void setProductID(long productID) {
        this.productID = productID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return unitPrice;
    }

    public void setPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productID=" + productID +
                ", name='" + name + '\'' +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
