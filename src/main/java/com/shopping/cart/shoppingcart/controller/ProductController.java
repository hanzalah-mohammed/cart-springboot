package com.shopping.cart.shoppingcart.controller;

import com.shopping.cart.shoppingcart.dao.CartDao;
import com.shopping.cart.shoppingcart.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(path = "products")
public class ProductController {

    @Autowired
    private CartDao cartDao;

    @GetMapping
    public Map<Long, Product> getAllProducts(){
        return cartDao.getAllProducts();
    }
}
