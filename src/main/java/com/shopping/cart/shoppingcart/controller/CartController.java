package com.shopping.cart.shoppingcart.controller;

import com.shopping.cart.shoppingcart.model.CartItem;
import com.shopping.cart.shoppingcart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping
    public List<CartItem> getCartItems(){
        return cartService.getCartItems();
    }

    @GetMapping(path = "checkout")
    public double checkout(){
        return cartService.getTotalPrice();
    }

    @PostMapping(path = "add")
    public void addItemInCart(@RequestParam Long productID, @RequestParam(required = false) Integer quantity){
        cartService.addItemInCart(productID, quantity);
    }

    @PutMapping(path = "update")
    public void updateDetails(@RequestParam Long productID, @RequestParam Integer quantity){
        cartService.updateDetails(productID, quantity);
    }

    @DeleteMapping(path = "remove")
    public void removeItem(@RequestParam Long productID){
        cartService.removeItem(productID);
    }

    @DeleteMapping(path = "remove-all")
    public void removeItemAll(@RequestParam Long productID){
        cartService.removeItemAll(productID);
    }

    @DeleteMapping(path = "clear")
    public void clearCart(){
        cartService.clearCart();
    }
}
