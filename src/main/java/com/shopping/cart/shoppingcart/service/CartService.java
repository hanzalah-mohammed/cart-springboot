package com.shopping.cart.shoppingcart.service;

import com.shopping.cart.shoppingcart.dao.CartDao;
import com.shopping.cart.shoppingcart.model.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService {

    @Autowired
    private CartDao cartDao;

    public List<CartItem> getCartItems() {
        return cartDao.getCartItems();
    }

    public void addItemInCart(Long productID, Integer quantity) {
        cartDao.addItemInCart(productID, quantity);
    }

    public void updateDetails(Long productID, Integer quantity){
        cartDao.updateDetails(productID, quantity);
    }

    public void removeItem(Long productID) {
        cartDao.removeItem(productID);
    }

    public void removeItemAll(Long productID) {
        cartDao.removeItemAll(productID);
    }

    public void clearCart() {
        cartDao.clearCart();
    }

    public double getTotalPrice(){
        return cartDao.getTotalPrice();
    }
}